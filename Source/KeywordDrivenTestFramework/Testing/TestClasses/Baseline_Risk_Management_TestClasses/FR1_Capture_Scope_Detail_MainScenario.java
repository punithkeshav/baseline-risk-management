/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1- Capture Scope Detail - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Scope_Detail_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Scope_Detail_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToBaselineRiskManagement())
        {
            return narrator.testFailed("Navigate To Baseline Risk Management Failed due :" + error);
        }
        if (!CaptureScopeDetail())
        {
            return narrator.testFailed("Capture Scope Detail Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Scope Detail");
    }

    public boolean NavigateToBaselineRiskManagement()
    {
        //Navigate to 
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagementTab()))
        {
            error = "Failed to wait for Baseline Risk Management tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagementTab()))
        {
            error = "Failed to click on Baseline Risk Management tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Baseline Risk Management tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagement_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagement_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureScopeDetail()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagement_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagement_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.businessUnitDropDown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.businessUnitDropDown()))
        {
            error = "Failed to click 'Business Unit' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2(), getData("Business unit")))
//        {
//            error = "Failed to wait for Business Unit option :" + getData("Business unit");
//            return false;
//        }
//       if (!SeleniumDriverInstance.pressEnter_2(Baseline_Risk_Management_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Business unit"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//        {
//            error = "Failed to wait for Business Unit Option drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//        {
//            error = "Failed to wait for Business Unit Option drop down";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Title()))
        {
            error = "Failed to enter Title :" + getData("Title");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Title(), getData("Title")))
        {
            error = "Failed to enter Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title :" + getData("Title"));

        //Introduction
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Introduction()))
        {
            error = "Failed to enter Introduction :" + getData("Introduction");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Introduction(), getData("Introduction")))
        {
            error = "Failed to enter Introduction :" + getData("Introduction");
            return false;
        }
        narrator.stepPassedWithScreenShot("Introduction :" + getData("Introduction"));

        //Objectives
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Objectives()))
        {
            error = "Failed to enter Objectives :" + getData("Objectives");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Objectives(), getData("Objectives")))
        {
            error = "Failed to enter Objectives  :" + getData("Objectives");
            return false;
        }
        narrator.stepPassedWithScreenShot("Objectives :" + getData("Objectives"));

        //Boundaries
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Boundaries()))
        {
            error = "Failed to enter Boundaries :" + getData("Boundaries");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Boundaries(), getData("Objectives")))
        {
            error = "Failed to enter  Boundaries :" + getData("Boundaries");
            return false;
        }
        narrator.stepPassedWithScreenShot("Boundaries :" + getData("Boundaries"));

        //Assumptions
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Assumptions()))
        {
            error = "Failed to enter Assumptions :" + getData("Assumptions");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Assumptions(), getData("Assumptions")))
        {
            error = "Failed to enter Assumptions  :" + getData("Assumptions");
            return false;
        }
        narrator.stepPassedWithScreenShot("Assumptions :" + getData("Assumptions"));

        //Methodology
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Methodology()))
        {
            error = "Failed to enter Methodology :" + getData("");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Methodology(), getData("Methodology")))
        {
            error = "Failed to enter  Methodology :" + getData("Methodology");
            return false;
        }
        narrator.stepPassedWithScreenShot("Methodology :" + getData("Methodology"));
        
//         //Team
//        
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to wait for Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to click Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Team Name text box";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2(), getData("Team Name")))
//        {
//            error = "Failed to enter  Team Name :" + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to wait for Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to click Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Team Name :" + getData("Team Name"));
        
        

        if (getData("Execute").equalsIgnoreCase("True"))
        {

            //Save
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            // SeleniumDriverInstance.pause(4000);
            String saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        } else
        {
            UploadDocument();
        }

        return true;
    }

    public boolean UploadDocument()
    {

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.linkToADocument_2()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.linkToADocument_2()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.UrlInput_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.UrlInput_TextArea(), getData("Document url")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.tile_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.tile_TextArea(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.linkADoc_Add_button()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.linkADoc_Add_button()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Baseline_Risk_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
