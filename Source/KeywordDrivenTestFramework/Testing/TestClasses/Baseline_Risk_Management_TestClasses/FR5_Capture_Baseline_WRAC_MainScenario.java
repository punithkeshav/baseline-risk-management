/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR5- Capture Baseline WRAC - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_Capture_Baseline_WRAC_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR5_Capture_Baseline_WRAC_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!NavigateToRegister())
        {
            return narrator.testFailed("Capture Baseline WRAC Failed Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Capture Baseline WRAC");
    }

    public boolean NavigateToRegister()
    {

        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close_DropDown()))
        {
//            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close_DropDown()))
//            {
//                error = "Failed to wait for drop down button save and close drop down";
//                return false;
//            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close_DropDown()))
            {
                error = "Failed to click button save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close()))
            {
                error = "Failed to wait for save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close()))
            {
                error = "Failed to click  save and close";
                return false;
            }

            narrator.stepPassedWithScreenShot("Clicked save and close");

            pause(5000);

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.RegisterTab()))
        {
            error = "Failed to wait for Register tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.RegisterTab()))
        {
            error = "Failed to click on Register tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Register tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Baseline_WRAC_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Baseline_WRAC_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Hazard / risk source classification
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HazardRiskSourceClassificationDropDown2()))
        {
            error = "Failed to wait for Hazard / risk source classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HazardRiskSourceClassificationDropDown2()))
        {
            error = "Failed to click Hazard / risk source classification dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Hazard / risk source classification text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2(), getData("Hazard risk source classification")))
        {
            error = "Failed to wait for Related activities option :" + getData("Hazard risk source classification");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard risk source classification"))))
        {
            error = "Failed to wait for Hazard risk source classification drop down option : " + getData("Hazard risk source classification");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
//        {
//            error = "Failed to wait to expand Hazard risk source classification";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
//        {
//            error = "Failed to expand Hazard risk source classification";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard risk source classification"))))
        {
            error = "Failed to wait for Hazard risk source classification drop down option:" + getData("Hazard risk source classification");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard risk source classification"))))
        {
            error = "Failed to wait for Hazard risk source classification Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Hazard risk source classification  : " + getData("Hazard risk source classification"));
        //Hazard / risk source description
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HazardrisksourcedescriptionDropDown()))
        {
            error = "Failed to wait for Functional ownership dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HazardrisksourcedescriptionDropDown()))
        {
            error = "Failed to click Functional ownership dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Functional ownership text box.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2(), getData("Hazard / risk source description")))
        {
            error = "Failed to enter text into Hazard / risk source description";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard / risk source description"))))
        {
            error = "Failed to wait for Hazard / risk source description:" + getData("Hazard / risk source description");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard / risk source description"))))
        {
            error = "Failed to wait for Hazard / risk source description Option drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Hazard / risk source description  : " + getData("Hazard / risk source description"));
        //Description of unwanted event
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.DescriptionofUnwantedEvent(), getData("Description of unwanted event")))
        {
            error = "Failed to enter text into Description of unwanted event";
            return false;
        }
        narrator.stepPassedWithScreenShot("Description of unwanted event  : " + getData("Description of unwanted event"));
        //Functional ownership
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.FunctionalOwnershipDropDown()))
        {
            error = "Failed to wait for Functional ownership dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.FunctionalOwnershipDropDown()))
        {
            error = "Failed to click Functional ownership dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Functional ownership text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Functional ownership"))))
        {
            error = "Failed to wait for Functional ownership :" + getData("Functional ownership");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Functional ownership"))))
        {
            error = "Failed to wait for Functional ownership Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Functional ownership : " + getData("Functional ownership"));
        //Current controls
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.CurrentControls(), getData("Current controls")))
        {
            error = "Failed to enter text into Current controls";
            return false;
        }
        narrator.stepPassedWithScreenShot("Current controls  : " + getData("Current controls"));
        //Likelihood
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.LikelihoodDropDown()))
        {
            error = "Failed to wait for Likelihood dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.LikelihoodDropDown()))
        {
            error = "Failed to click Likelihood dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Likelihood text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Likelihood"))))
        {
            error = "Failed to wait for Likelihood :" + getData("Likelihood");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Likelihood"))))
        {
            error = "Failed to wait for Likelihood Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Likelihood : " + getData("Likelihood"));
        ///(S)
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.SDropDown()))
        {
            error = "Failed to wait for Safety dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.SDropDown()))
        {
            error = "Failed to click Safety dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Safety text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Safety"))))
        {
            error = "Failed to wait for Safety :" + getData("Safety");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Safety"))))
        {
            error = "Failed to wait for Safety Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Safety : " + getData("Safety"));
        //Health
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HDropDown()))
        {
            error = "Failed to wait for Health dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HDropDown()))
        {
            error = "Failed to click Health dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Health text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text5(getData("Health"))))
        {
            error = "Failed to wait for Health :" + getData("Health");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text5(getData("Health"))))
        {
            error = "Failed to wait for H Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("H : " + getData("H"));
        //Environment

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.EDropDown()))
        {
            error = "Failed to wait for E dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.EDropDown()))
        {
            error = "Failed to click E dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for E text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Environment"))))
        {
            error = "Failed to wait for E:" + getData("Environment");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Environment"))))
        {
            error = "Failed to wait for E Option drop down";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("E1"))))
//        {
//            error = "Failed to wait for E:" + getData("E1");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("E1"))))
//        {
//            error = "Failed to wait for E Option drop down :" + getData("E1");
//            return false;
//        }

        narrator.stepPassedWithScreenShot("E : " + getData("E"));
        //Community
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.CDropDown()))
        {
            error = "Failed to wait for C dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.CDropDown()))
        {
            error = "Failed to click C dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for C text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Community"))))
        {
            error = "Failed to wait for C:" + getData("C");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Community"))))
        {
            error = "Failed to wait for C Option drop down";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("C1"))))
//        {
//            error = "Failed to wait for C :" + getData("C1");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("C1"))))
//        {
//            error = "Failed to wait for C Option drop down";
//            return false;
//        }

        narrator.stepPassedWithScreenShot("Community : " + getData("Community"));
        //L&R

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.LRDropDown()))
        {
            error = "Failed to wait for LR dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.LRDropDown()))
        {
            error = "Failed to click LR dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for LR text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Legal and Regulatory"))))
        {
            error = "Failed to wait for LR:" + getData("LR");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Legal and Regulatory"))))
        {
            error = "Failed to wait for LR Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Legal and Regulatory : " + getData("Legal and Regulatory"));
        //M

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.MDropDown()))
        {
            error = "Failed to wait for Material loss dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.MDropDown()))
        {
            error = "Failed to click Material loss dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Material loss text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Material loss"))))
        {
            error = "Failed to wait for Material loss :" + getData("Material loss");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Material loss"))))
        {
            error = "Failed to wait for Material loss Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Material loss : " + getData("Material loss"));

        //Reputation
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.RDropDown()))
        {
            error = "Failed to wait for Reputation dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.RDropDown()))
        {
            error = "Failed to click Reputation dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Reputation text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Reputation"))))
        {
            error = "Failed to wait for Reputation :" + getData("Reputation");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text4(getData("Reputation"))))
        {
            error = "Failed to wait for Reputation Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Reputation : " + getData("Reputation"));

        //Risk rating 
        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HazardInventory_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HazardInventory_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }

}
