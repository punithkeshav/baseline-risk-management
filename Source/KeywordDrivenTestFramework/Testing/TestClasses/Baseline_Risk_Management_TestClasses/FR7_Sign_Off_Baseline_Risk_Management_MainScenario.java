/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR7 Sign Off Baseline Risk Management Main Scenario",
        createNewBrowserInstance = false
)
public class FR7_Sign_Off_Baseline_Risk_Management_MainScenario extends BaseClass
{

    String error = "";

    public FR7_Sign_Off_Baseline_Risk_Management_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
//        if (!DeleteBaseline())
//        {
//            return narrator.testFailed("Failed To Delete Audit :" + error);
//        }

        return narrator.finalizeTest("Successfully Audit Deleted");
    }
    
}
