/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR2- Capture Baseline Change Log - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Baseline_Change_Log_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Baseline_Change_Log_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!addChecklist())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Saved Record");
    }

    public boolean addChecklist()
    {

        //Baseline Change Log
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineChangeLog_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaselineChangeLog_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked Baseline Change Log Add button");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineChangeLog_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaselineChangeLog_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Date of change
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.DateOfChange()))
        {
            error = "Failed to wait for Date of change";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.DateOfChange(), startDate))
        {
            error = "Failed to enter Date of change";
            return false;
        }
        narrator.stepPassedWithScreenShot("Date of change :" + startDate);

        //Reasons for review include dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.ReasonsForReviewInclude_dropdown()))
        {
            error = "Failed to wait for Reasons for review include dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.ReasonsForReviewInclude_dropdown()))
        {
            error = "Failed to click Reasons for review include dropdown";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click Reasons for review include dropdown.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.ReasonsForReviewInclude_CheckBox(getData("Reasons for review include"))))
        {
            error = "Failed to wait for Reasons for review include :" + getData("Reasons for review include");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByJavascript(Baseline_Risk_Management_PageObjects.ReasonsForReviewInclude_CheckBox(getData("Reasons for review include"))))
        {
            error = "Failed to enter Reasons for review include :" + getData("Reasons for review include");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.ReasonsForReviewInclude_dropdown()))
        {
            error = "Failed to click Reasons for review include dropdown";
            return false;
        }

        //Comments
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Comments()))
        {
            error = "Failed to wait for Comments";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Comments(), getData("Comments")))
        {
            error = "Failed to enter Comments";
            return false;
        }

        narrator.stepPassedWithScreenShot("Comments :" + startDate);

        //Person responsible for Sign off
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PersonResponsibleDropDown()))
        {
            error = "Failed to wait for Person responsible for Sign off drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.PersonResponsibleDropDown()))
        {
            error = "Failed to click Person responsible for Sign off drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person responsible for Sign off text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch(), getData("Person responsible for Sign off")))
        {
            error = "Failed to enter Person responsible for Sign off  option :" + getData("Person responsible for Sign off");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(Baseline_Risk_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Person responsible for Sign off"))))
        {
            error = "Failed to wait for Validator  drop down option : " + getData("Person responsible for Sign off");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Person responsible for Sign off"))))
        {
            error = "Failed to click Person responsible for Sign off drop down option : " + getData("Person responsible for Sign off");
            return false;
        }

        narrator.stepPassed("Person responsible for Sign off  :" + getData("Person responsible for Sign off"));

        if (getData("Execute Link to audit").equalsIgnoreCase("True"))
        {
            //Link to audit
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.LinkToAuditDropDown()))
            {
                error = "Failed to wait for Link to audit drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementByJavascript(Baseline_Risk_Management_PageObjects.LinkToAuditDropDown()))
            {
                error = "Failed to click Link to audit drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Link to audit"))))
            {
                error = "Failed to wait for Link to audit drop down option : " + getData("Link to audit");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementByJavascript(Baseline_Risk_Management_PageObjects.Text(getData("Link to audit"))))
            {
                error = "Failed to click Link to audit drop down option : " + getData("Link to audit");
                return false;
            }
            narrator.stepPassedWithScreenShot("Link to audit :" + getData("Link to audit"));

        }

        if (getData("Execute Link to event").equalsIgnoreCase("True"))
        {
            //Link to audit
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.LinkToEventDropDown()))
            {
                error = "Failed to wait for Link to event drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementByJavascript(Baseline_Risk_Management_PageObjects.LinkToEventDropDown()))
            {
                error = "Failed to click Link to event drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text2(getData("Link to event"))))
            {
                error = "Failed to wait for Link to event drop down option : " + getData("Link to event");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementByJavascript(Baseline_Risk_Management_PageObjects.Text2(getData("Link to event"))))
            {
                error = "Failed to click Link to event drop down option : " + getData("Link to event");
                return false;
            }
            narrator.stepPassedWithScreenShot("Link to event :" + getData("Link to event"));

        }

        //Link to stakeholder engagement
        if (getData("Execute Link to stakeholder engagement").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.LinkTostakeholderEngagementDropDown()))
            {
                error = "Failed to wait for Link to stakeholder engagement drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementByJavascript(Baseline_Risk_Management_PageObjects.LinkTostakeholderEngagementDropDown()))
            {
                error = "Failed to click Link to stakeholder engagement drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Link to stakeholder engagement"))))
            {
                error = "Failed to wait for Link to stakeholder engagement drop down option :" + getData("Link to stakeholder engagement");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementByJavascript(Baseline_Risk_Management_PageObjects.Text(getData("Link to stakeholder engagement"))))
            {
                error = "Failed to click Link to stakeholder engagement drop down option :" + getData("Link to stakeholder engagement");
                return false;
            }
            narrator.stepPassedWithScreenShot("Link to stakeholder engagement :" + getData("Link to stakeholder engagement"));

        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Baseline_Change_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Baseline_Change_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }
}
