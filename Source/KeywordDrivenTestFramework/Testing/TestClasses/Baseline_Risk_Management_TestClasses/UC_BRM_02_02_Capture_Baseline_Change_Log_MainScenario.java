/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "UC BRM 02 02 Capture Baseline Change Log Main Scenario",
        createNewBrowserInstance = false
)

public class UC_BRM_02_02_Capture_Baseline_Change_Log_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_BRM_02_02_Capture_Baseline_Change_Log_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureCompleted())
        {
            return narrator.testFailed("Capture Completed Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Capture Inspection");
    }

    public boolean CaptureCompleted()
    {

        //Completed
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.CompletedDropDown()))
        {
            error = "Failed to wait for Completed drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.CompletedDropDown()))
        {
            error = "Failed to click Completed drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Completed"))))
        {
            error = "Failed to wait for Completed drop down option :" + getData("Completed");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Completed"))))
        {
            error = "Failed to click Completed drop down option :" + getData("Completed");
            return false;
        }
        narrator.stepPassedWithScreenShot("Completed :" + getData("Completed"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Baseline_Change_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Baseline_Change_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close_DropDown()))
            {
                error = "Failed to wait for drop down button save and close drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close_DropDown()))
            {
                error = "Failed to click button save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close()))
            {
                error = "Failed to wait for save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close()))
            {
                error = "Failed to click  save and close";
                return false;
            }

            narrator.stepPassedWithScreenShot("Clicked save and close");

            pause(5000);

            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.RegisterTab()))
            {
                error = "Failed to wait for Register tab.";
                return false;
            }
        }

        return true;
    }

}
