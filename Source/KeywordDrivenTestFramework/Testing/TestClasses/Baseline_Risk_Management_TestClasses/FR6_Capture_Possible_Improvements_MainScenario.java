/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR6-Capture Possible Improvements Main Scenario",
        createNewBrowserInstance = false
)
public class FR6_Capture_Possible_Improvements_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_Capture_Possible_Improvements_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Register())
        {
            return narrator.testFailed("Register Failed due To : " + error);
        }

        return narrator.finalizeTest("Successfully Capture Actions");
    }

    public boolean Register()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.formview()))
        {
            error = "Failed to wait for the form icon";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.formview()))
        {
            error = "Failed to click the form icon";
            return false;
        }

        pause(5000);

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaslineWracProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaslineWracProcessFlow()))
        {
            error = "Failed to click  process flow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked process flow button");

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsOrAdditionalControlsDropDown()))
        {
            error = "Failed to wait for Possible Improvements or Additional Controls Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsOrAdditionalControlsDropDown()))
        {
            error = "Failed to click  Possible Improvements or Additional Controls Drop Down";
            return false;
        }

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsOrAdditionalControlsAddButton()))
        {
            error = "Failed to wait for Possible Improvements or Additional Controls Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsOrAdditionalControlsAddButton()))
        {
            error = "Failed to click  Possible Improvements or Additional Controls Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked Add Button", "");

        //Possible Improvements or Additional Controls Actions Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsProcessFlow()))
        {
            error = "Failed to click  process flow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked process flow button");

        //Type of action
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeOfAction()))
        {
            error = "Failed to wait for Type of action";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.TypeOfAction()))
        {
            error = "Failed to wait for Type of action";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }

        //Action description
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.ActionDescription(), getData("Action description")))
        {
            error = "Failed to wait for Action description";
            return false;
        }

        narrator.stepPassedWithScreenShot("Action description :" + getData("Action description"));

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Entity()))
        {
            error = "Failed to wait for Entity";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Entity()))
        {
            error = "Failed to wait for Entity";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2(), getData("Business Unit")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business Unit");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Business unit"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Business Unit Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        pause(2000);
        //Responsible person

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PosibleImrovementsResponsibleperson()))
        {
            error = "Failed to wait for Responsible person drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.PosibleImrovementsResponsibleperson()))
        {
            error = "Failed to click Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person :" + getData("Responsible person");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(Baseline_Risk_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }

        //Agency
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsAgency()))
        {
            error = "Failed to wait for Agency";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsAgency()))
        {
            error = "Failed to click Agency";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Agency"))))
        {
            error = "Failed to wait for Agency drop down option : " + getData("Agency");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Agency"))))
        {
            error = "Failed to click Agency drop down option : " + getData("Agency");
            return false;
        }
        narrator.stepPassedWithScreenShot("Agency :" + getData("Agency"));

        //Action due date
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsActionDueDate()))
        {
            error = "Failed to wait for Action due date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.PossibleImprovementsActionDueDate(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        //save
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.PossibleImprovements_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.PossibleImprovements_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

         //SeleniumDriverInstance.pause(2000);
        String saved = "";

//        if (!SeleniumDriverInstance.waitForElementNoLongerPresentByXpath(Baseline_Risk_Management_PageObjects.SavingMask()))
//        {
//            error = "Failed to wait for Saving mask to be no longer present";
//            return false;
//        }

        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup_1()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup_1());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }

}
