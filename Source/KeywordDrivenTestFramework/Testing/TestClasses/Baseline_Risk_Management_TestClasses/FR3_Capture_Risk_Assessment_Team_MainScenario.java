/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR3 - Capture Risk Assessment Team Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Risk_Assessment_Team_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Risk_Assessment_Team_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Risk_Assessment_Team())
        {
            return narrator.testFailed("Risk Assessment Team Failed due : " + error);
        }

        return narrator.finalizeTest("Successfully Capture Risk Assessment Team");
    }

    public boolean Risk_Assessment_Team()
    {

        //Risk Assessment Team Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Risk_Assessment_Team_Add()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Risk_Assessment_Team_Add()))
        {
            error = "Failed to click on Add button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked Risk Assessment Team Add button");

        //Full name
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.FullNameDropDown()))
        {
            error = "Failed to wait for Full name dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.FullNameDropDown()))
        {
            error = "Failed to click Full name dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Full name text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch(), getData("Full name")))
        {
            error = "Failed to wait for Full name option :" + getData("Full name");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(Baseline_Risk_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text2(getData("Full name"))))
        {
            error = "Failed to wait for Full name drop down option : " + getData("Full name");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text2(getData("Full name"))))
        {
            error = "Failed to click  Full name drop down option : " + getData("Full name");
            return false;
        }

        narrator.stepPassedWithScreenShot("Full name :" + getData("Full name"));

        //Experience/Role
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.ExperienceRoleDropDown()))
        {
            error = "Failed to wait for Experience/Role drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.ExperienceRoleDropDown()))
        {
            error = "Failed to click Experience/Role drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Experience/Role"))))
        {
            error = "Failed to wait for Experience/Role drop down option :" + getData("Experience/Role");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Experience/Role"))))
        {
            error = "Failed to click Experience/Role drop down option :" + getData("Experience/Role");
            return false;
        }
        narrator.stepPassedWithScreenShot("Experience/Role :" + getData("Experience/Role"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Baseline_Change_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Baseline_Change_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
