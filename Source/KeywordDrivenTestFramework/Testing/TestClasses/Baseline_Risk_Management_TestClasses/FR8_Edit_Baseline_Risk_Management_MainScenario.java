/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;

/**
 *
 * @author SMabe
 */

@KeywordAnnotation(
        Keyword = "FR8-Edit Baseline Risk Management Main Scenario",
        createNewBrowserInstance = false
)
public class FR8_Edit_Baseline_Risk_Management_MainScenario extends BaseClass
{

    String error = "";

    public FR8_Edit_Baseline_Risk_Management_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteBaseline())
        {
            return narrator.testFailed("Failed To Delete Audit :" + error);
        }

        return narrator.finalizeTest("Successfully Audit Deleted");
    }
    
    
    public boolean DeleteBaseline() throws InterruptedException
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");
        
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Basline_Save_And_Close_DropDown()))
        {
            error = "Failed to wait for drop down button save and close drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Basline_Save_And_Close_DropDown()))
        {
            error = "Failed to click button save and close drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Basline_Save_And_Close()))
        {
            error = "Failed to wait for save and close drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Basline_Save_And_Close()))
        {
            error = "Failed to click  save and close";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked save and close");

        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

         if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagementTab()))
        {
            error = "Failed to wait for Baseline Risk Management tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagementTab()))
        {
            error = "Failed to click on Baseline Risk Management tab.";
            return false;
        }
        pause(3000);

        narrator.stepPassedWithScreenShot("Basline Risk Management Tab Clicked Successfully");
        
        
        

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagement_Add()))
        {
            error = "Failed to locate Waste Management add button";
            return false;
        }  

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :"+array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
         pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

           narrator.stepPassedWithScreenShot("Record Found and clicked record : "+array[1]);
          
        pause(5000);
        
         //Introduction
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Introduction()))
        {
            error = "Failed to enter Introduction :" + getData("Introduction");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Introduction(), getData("Introduction")))
        {
            error = "Failed to enter Introduction :" + getData("Introduction");
            return false;
        }
        narrator.stepPassedWithScreenShot("Introduction :" + getData("Introduction"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
        
    }
}
