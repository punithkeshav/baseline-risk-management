/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;

/**
 *
 * @author SMabe
 */

@KeywordAnnotation(
        Keyword = "FR9-Delete Baseline Risk Management Main Scenario",
        createNewBrowserInstance = false
)
public class FR9_Delete_Baseline_Risk_Management_MainScenario extends BaseClass
{

    String error = "";

    public FR9_Delete_Baseline_Risk_Management_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteBaseline())
        {
            return narrator.testFailed("Failed To Delete Audit :" + error);
        }

        return narrator.finalizeTest("Successfully Audit Deleted");
    }
    
    
    public boolean DeleteBaseline() throws InterruptedException
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

         if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagementTab()))
        {
            error = "Failed to wait for Baseline Risk Management tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagementTab()))
        {
            error = "Failed to click on Baseline Risk Management tab.";
            return false;
        }
        pause(3000);

        narrator.stepPassedWithScreenShot("Basline Risk Management Tab Clicked Successfully");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BaselineRiskManagement_Add()))
        {
            error = "Failed to locate Basline Risk Management add button";
            return false;
        }
        

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :"+array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
         pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

       narrator.stepPassedWithScreenShot("Record Found and clicked record : "+array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.DeleteButton()))
        {
            error = "Failed to wait for delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }


        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }
        narrator.stepPassedWithScreenShot("");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);
        return true;
        
    }
}
