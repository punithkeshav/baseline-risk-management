/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Baseline_Risk_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.Baseline_Risk_Management_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Baseline_Risk_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR4 - Capture Hazard Inventory Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Hazard_Inventory_Main_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Capture_Hazard_Inventory_Main_Scenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!HazardInventory())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Capture Actions");
    }

    public boolean HazardInventory()
    {             
         if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close_DropDown()))
            {
                error = "Failed to wait for drop down button save and close drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close_DropDown()))
            {
                error = "Failed to click button save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close()))
            {
                error = "Failed to wait for save and close drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Button_Save_And_Close()))
            {
                error = "Failed to click  save and close";
                return false;
            }

            narrator.stepPassedWithScreenShot("Clicked save and close");       
            pause(4000);
              
        //Hazard Inventory

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HazardInventoryTab()))
        {
            error = "Failed to wait for Hazard Inventory tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HazardInventoryTab()))
        {
            error = "Failed to click on Hazard Inventory tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Hazard Inventory tab.");

        //Hazard Inventory Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HazardInventory_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HazardInventory_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked Hazard Inventory Add button");

        //Hazard / risk source classification
         if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HazardRiskSourceClassificationDropDown()))
        {
            error = "Failed to wait for Hazard / risk source classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HazardRiskSourceClassificationDropDown()))
        {
            error = "Failed to click Hazard / risk source classification dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Hazard / risk source classification text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2(), getData("Hazard risk source classification")))
        {
            error = "Failed to wait for Related activities option :" + getData("Hazard risk source classification");
            return false;
        }
     if (!SeleniumDriverInstance.pressEnter_2(Baseline_Risk_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard risk source classification"))))
        {
            error = "Failed to wait for Hazard risk source classification drop down option : " + getData("Hazard risk source classification");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
//        {
//            error = "Failed to wait to expand Hazard risk source classification";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
//        {
//            error = "Failed to expand Hazard risk source classification";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard risk source classification 1"))))
//        {
//            error = "Failed to wait for :" + getData("Hazard risk source classification 1");
//            return false;
//        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Hazard risk source classification"))))
        {
            error = "Failed to wait for Hazard risk source classification Option drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Hazard risk source classification  : " + getData("Hazard risk source classification"));
        
        
        
        //Hazard / risk source description
          if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.HazardRiskSourceDescription(), getData("Hazard / risk source description")))
        {
            error = "Failed to enter text into Hazard / risk source description";
            return false;
        }
        narrator.stepPassedWithScreenShot("Hazard / risk source description  : " + getData("Hazard / risk source description"));
        
        
        
        //Related activities
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.RelatedActivitiesDropDown()))
//        {
//            error = "Failed to wait for Related activities dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.RelatedActivitiesDropDown()))
//        {
//            error = "Failed to click Related activities dropdown.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Related activities text box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.TypeSearch2(), getData("Related activities")))
//        {
//            error = "Failed to wait for Related activities option :" + getData("Related activities");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter())
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Related activities"))))
//        {
//            error = "Failed to wait for Related activities drop down option : " + getData("Related activities");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
//        {
//            error = "Failed to wait to expand Related activities";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.BusinessUnitexpandButton()))
//        {
//            error = "Failed to expand Related activities";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Related activities 1"))))
//        {
//            error = "Failed to wait for :" + getData("Related activities 1");
//            return false;
//        }
//
//        pause(3000);
//        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Related activities 1"))))
//        {
//            error = "Failed to wait for Related activities Option drop down";
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Related activities 1 : " + getData("Related activities 1"));

        //Magnitude of hazard / aspect
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.MagnitudeofHazard_Aspect(), getData("Magnitude of hazard / aspect")))
        {
            error = "Failed to enter text into Magnitude of hazard / aspect";
            return false;
        }
        narrator.stepPassedWithScreenShot("Magnitude of hazard / aspect  : " + getData("Magnitude of hazard / aspect"));
        //Mechanism of release
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.MechanismofRelease(), getData("Mechanism of release")))
        {
            error = "Failed to enter text into Mechanism of release";
            return false;
        }
        narrator.stepPassedWithScreenShot("Mechanism of release  : " + getData("Mechanism of release"));

        //Assumptions / uncertainty
        if (!SeleniumDriverInstance.enterTextByXpath(Baseline_Risk_Management_PageObjects.Assumptions_Uncertainty(), getData("Assumptions / uncertainty")))
        {
            error = "Failed to enter text into Assumptions / uncertainty.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Assumptions / uncertainty  : " + getData("Assumptions / uncertainty"));

        //Issue based required
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.IssueBasedRequiredDropDown()))
        {
            error = "Failed to wait for Issue based required drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.IssueBasedRequiredDropDown()))
        {
            error = "Failed to click Issue based required drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.Text(getData("Issue based required"))))
        {
            error = "Failed to wait for Issue based required drop down option :" + getData("Issue based required");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.Text(getData("Issue based required"))))
        {
            error = "Failed to click Issue based required drop down option :" + getData("Issue based required");
            return false;
        }
        narrator.stepPassedWithScreenShot("Issue based required :" + getData("Issue based required"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.HazardInventory_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Baseline_Risk_Management_PageObjects.HazardInventory_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        SeleniumDriverInstance.pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Baseline_Risk_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Baseline_Risk_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Baseline_Risk_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Baseline_Risk_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
